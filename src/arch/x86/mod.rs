pub mod loader;

#[cfg(not(test))]
pub mod init;

#[cfg(test)]
#[no_mangle]
pub extern "C" fn main() {
    crate::test_main();
}
