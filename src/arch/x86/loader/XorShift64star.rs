use get_random_const::random;

pub struct XorShift64star {
    state: u64
}

/**
 * XorShift64* PRNG from https://en.wikipedia.org/wiki/Xorshift#xorshift*
 * Modified to only return the higher 32-bits from the 64-bits state to increase security
 */
pub trait XorShift64starImpl {
    fn new() -> Self;
    fn reseed(&mut self, new_seed: u64);
    fn get_random(&mut self) -> u32;
}

impl XorShift64starImpl for XorShift64star {
    fn new() -> Self {
        /*
         * As we are in a x86 system, we are able to use some RAM content that represent the current
         * state of the booting system, the firmware configuration and version, how much the computer
         * as already worked, etc...
         * Those RAM data aren't random but provide us with a runtime environment that may be a bit
         * different on each boot depending on the firmware boot process. We could combine those data
         * with our compile-time random constant to get a week seed that is specific to this specific
         * combination of firmware version and kernel version.
         */

        // Extract some data from the BIOS BDA (or equivalent if newer system are in use)
        let ebda_base_addr = 0x040E as *const u16; // Get the (usual) EBDA base address from the BDA
        let ebda_padding = 0x0413 as *const u16; // Get the number of kilobytes before EBDA
        let irq0 = 0x046C as *const u16; // Number of IRQ0 timer ticks since boot

        // Extract some random and unknown data from the EBDA
        let unknown_ebda = 0x8ADDC as *const u32;

        // Get some random and unknown data from somewhere in the memory
        let random_data1 = 0x7D00 as *const u32;
        let random_data2 = 0x6FAB as *const u32;

        // Combine those data to create a week representation of the current hardware setup (runtime)
        let mut ram_content: u64;
        unsafe {
            ram_content = (*random_data1 as u64) << 32;
            ram_content += (*random_data2) as u64;

            let mut ebda = (*unknown_ebda as u64) << 32;
            ebda += (*ebda_base_addr as u64) << 16;
            ebda += (*ebda_padding) as u64;
            ram_content ^= ebda;

            ram_content = if *irq0 > 0 {
                ram_content.overflowing_mul(*irq0 as u64).0
            } else {
                ram_content.overflowing_mul(random!(u16) as u64).0
            }
        };

        // Compiletime random value
        let compile_time_seed: u64 = random!(u64);

        Self {
            state: compile_time_seed ^
                ((compile_time_seed.rotate_right(16).overflowing_add(ram_content).0)
                    ^ (compile_time_seed.rotate_left(16) & ram_content))
        }
    }

    fn reseed(&mut self, new_seed: u64) {
        self.state = new_seed;
    }

    fn get_random(&mut self) -> u32 {
        self.state ^= self.state >> 12;
        self.state ^= self.state << 25;
        self.state ^= self.state >> 27;
        ((self.state.overflowing_mul(0x2545F4914F6CDD1D).0) >> 32) as u32
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const EBDA_BASE_ADDR: u16 = 0xe194;
    const EBDA_PADDING: u16 = 0xd628;
    const UNKNOWN_EBDA: u32 = 0xa87b2e52;
    const RANDOM_DATA1: u32 = 0x37adcfac;
    const RANDOM_DATA2: u32 = 0xa2e84ab2;
    const COMPILE_TIME_SEED: u64 = random!(u64);

    const SEED: u64 = {
        let mut init: u64 = (RANDOM_DATA1 as u64) << 32;
        init += RANDOM_DATA2 as u64;
        let mut ebda: u64 = (UNKNOWN_EBDA as u64) << 32;
        ebda += (EBDA_BASE_ADDR as u64) << 16;
        ebda += EBDA_PADDING as u64;
        init ^= ebda;
        init = init.overflowing_mul(16).0;
        COMPILE_TIME_SEED ^ ((COMPILE_TIME_SEED.rotate_right(16).overflowing_add(init).0) ^ (COMPILE_TIME_SEED.rotate_left(16) & init))
    };

    #[substance_test]
    fn test_same_result_from_same_seed() {
        let mut random1 = XorShift64star {
            state: SEED
        };
        let mut random2 = XorShift64star {
            state: SEED
        };

        sf_assert_eq!(random1.get_random(), random2.get_random(), "Two XorShift64* should give the same result with the same seed");
        sf_assert_eq!(random1.get_random(), random2.get_random(), "Two XorShift64* should give the same result with the same number of operations");
        let _ = random2.get_random();
        sf_assert_ne!(random1.get_random(), random2.get_random(), "Two XorShift64* should differ without the same number of operations");
    }

    #[substance_test]
    fn test_differ_with_different_seed() {
        let mut random1 = XorShift64star {
            state: SEED
        };
        let mut random2 = XorShift64star {
            state: SEED + 1
        };

        sf_assert_ne!(random1.get_random(), random2.get_random(), "Two XorShift64* should not give the same result with different seed");
    }

    #[substance_test]
    fn test_reseed_should_work_as_intended() {
        let mut random1 = XorShift64star {
            state: SEED
        };
        let mut random2 = XorShift64star {
            state: SEED
        };

        let result1 = random1.get_random();
        sf_assert_eq!(result1, random2.get_random());
        random2.reseed(SEED);
        sf_assert_eq!(result1, random2.get_random(), "If a reseed is done with the same seed, the sequence should restart");
        random2.reseed(SEED - 1);
        sf_assert_ne!(result1, random2.get_random(), "If a reseed is done with a different seed, the sequence shouldn't restart");
    }
}