use core::arch::{asm, global_asm};

//////////////////////////////////////////////

#[cfg(feature = "trap_bad_bootloader")]
global_asm!(concat!(include_str!("trap_16b.S"), include_str!("trap_32b.S"), include_str!("trap_64b.S")), options(att_syntax));

global_asm!(include_str!("entry.S"), options(att_syntax));

assert_obj_safe!(BootInfoParser);
assert_cfg!(all(not(all(feature = "multiboot2", feature = "stivale2")),
                any(    feature = "multiboot2", feature = "stivale2")),
            "Must exclusively use MultiBoot2 or Stivale2 as bootloader");

cfg_if::cfg_if! {
    if #[cfg(feature = "multiboot2")] {
        #[path = "./multiboot2.rs"] mod multiboot2;
        macro_rules! get_parser { () => { multiboot2::get_parser() }}
    } else {
        macro_rules! get_parser { () => {compile_error!("Missing bootloader feature") }}
    }
}

//////////////////////////////////////////////

#[derive(Debug, PartialEq)]
enum ErrorCode {
    NoError       = 0x00000000,
    InvalidAddr   = 0x01010101,
    UnalignedAddr = 0x02020202,
    InvalidInfo   = 0x03030303,
    InvalidSize   = 0x04040404,
    MMAPNotFound  = 0x05050505,
}

trait BootInfoParser {
    fn new() -> Self where Self: Sized;
    fn init(&mut self, _: u32) -> ErrorCode;
}

//////////////////////////////////////////////


#[no_mangle]
#[link_section = ".loader"]
pub extern "C" fn init_kernel(magic: u32, mbi: u32) -> u32 {
    let mut bip = get_parser!();
    let y = bip.init(mbi);
    match y {
        ErrorCode::NoError => {}
        _ => panic(y)
    }
    return mbi ^ magic;
}

#[link_section = ".loader"]
fn panic(_code: ErrorCode) -> ! {
    loop {
        unsafe {
            asm!("cli; hlt");
        }
    }
}
