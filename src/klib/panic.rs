use core::panic::PanicInfo;

#[cfg_attr(not(test), panic_handler)]
fn panic(_info: &PanicInfo) -> ! {
    // For now we do nothing and just loop
    loop {}
}
