use crate::arch::x86::init::ErrorCode;
use crate::arch::x86::init::BootInfoParser;

//////////////////////////////////////////////

const MMAP_TAG_TYPE :u32 = 6;
const END_TAG_TYPE :u32 = 0;

//////////////////////////////////////////////

/// MultiBoot2 tag header
///
/// # Constants
///
/// * `type_of_tag` is the type code of the current tag block
/// * `size_of_tag` is the size of the current tag from it's starting address
#[allow(dead_code)]
struct Tag {
    type_of_tag: u32,
    size_of_tag: u32
}

/// MultiBoot2 memory map block date
///
/// # Constants
///
/// * `base_addr` is the starting physical address
/// * `length`  is the size of the memory region in bytes
/// * `type_of_block` is the variety of address range represented, 1 is for free RAM
/// * `reserved` must be ignored
#[allow(dead_code)]
struct MMAPBlock {
    base_addr: u64,
    length: u64,
    type_of_block: u32,
    reserved :u32
}

/// MultiBoot2 memory map tag
///
/// # Constants
///
/// * `type_of_tag` @see the MultiBoot2 tag header
/// * `size_of_tag` @see the MultiBoot2 tag header
/// * `entry_size` specify the size for each mmap_block elements
/// * `entry_version` specify the current version for each mmap_block
/// * `entries` an array of mmap_block
#[allow(dead_code)]
struct MMAPTag<'a> {
    type_of_tag :u32,
    size_of_tag: u32,
    entry_size: u32,
    entry_version :u32,
    entries: &'a [MMAPBlock]
}

pub struct MultiBootInfo<'a> {
    mmap: *const MMAPTag<'a>
}

//////////////////////////////////////////////

impl BootInfoParser for MultiBootInfo<'_> {
    #[link_section = ".loader"]
    fn new() -> MultiBootInfo<'static> {
        MultiBootInfo { mmap: core::ptr::null::<MMAPTag>() }
    }

    #[link_section = ".loader"]
    fn init(&mut self, addr : u32) -> ErrorCode {
        let ptr = addr as *const u32;
        let size : u32;
        unsafe { size = *ptr; }
        let end_addr = addr + size;

        /* Check if the pointer (thus the structure) is plausible:
         * - self.ptr must be above 0
         * - self.ptr must be aligned (multiple of 8)
         * - self.ptr must be below 0xFFFFFF - self.size -1
         * - self.size must be at least 72 bytes as we expect minimum two mmap_block
         * We are forcibly trusting that the value for the pointer is a valid memory address
         */
        if addr == 0 {
            return ErrorCode::InvalidAddr;
        } else if addr % 8 != 0 {
            return ErrorCode::UnalignedAddr;
        } else if addr > 0xFFFFFFFF - size - 1 {
            return ErrorCode::InvalidInfo;
        } else if size < 72 {
            return ErrorCode::InvalidSize;
        }

        let mut current_tag_addr = addr;
        loop {
            let current_tag = current_tag_addr as *const Tag;

            // Test for NULL
            if current_tag.is_null() {
                return ErrorCode::InvalidInfo;
            }

            let next_tag_addr;
            unsafe {
                next_tag_addr = current_tag_addr + (*current_tag).size_of_tag;

                // Test for overflow or bad tag or  end of the MultiBoot2 structure or the MMAP
                if (*current_tag).size_of_tag < 8
                    || current_tag_addr >= end_addr
                    || current_tag_addr < addr {
                    return ErrorCode::InvalidInfo;
                } else if (*current_tag).type_of_tag == END_TAG_TYPE {
                    return ErrorCode::MMAPNotFound;
                } else if (*current_tag).type_of_tag == MMAP_TAG_TYPE {
                    break;
                }
            }

            current_tag_addr = next_tag_addr;
        }

        self.mmap = current_tag_addr as *const MMAPTag;
        return ErrorCode::NoError;
    }
}

//////////////////////////////////////////////

#[link_section = ".loader"]
pub fn get_parser() -> MultiBootInfo<'static> {
    let x: MultiBootInfo = BootInfoParser::new();
    return x;
}

//////////////////////////////////////////////

// #[cfg(test)]
// mod tests {
//     use super::*;
//
//     #[test]
//     fn null_ptr() {
//         let mut a = get_parser();
//         assert_eq!(a.init(0), ErrorCode::InvalideAddr);
//     }
// }
