/* Lower Memory Layout:
 * 0x00000-0x003FF     IVT (Interrupt Vector Table, can be override by kernel)
 * 0x00400-0x004FF     BDA (BIOS data area, can be override by kernel)
 * 0x00500-0x07BFF     [guaranteed free for use by the BIOS]
 * 0x07C00-0x07DFF     Bootloader (can be override by kernel)
 * 0x07E00-0x7FFFF     [guaranteed free for use by the BIOS]
 * 0x80000-0x9FBFF     (depending on EBDA size) ~int 0x12 => ax=from 0 to EBDA bottom~
 * 0x9FC00-0x9FFFF     EBDA (Extended BIOS Data Area) *KEEP*
 * 0xA0000-0xFFFFF     ROM area *KEEP*
 *     0xA0000-0xBFFFF     Video RAM
 *         0xA0000-0xAFFFF     VGA display mode
 *         0xB0000-0xB7FFF     Monochrome text mode buffer
 *         0xB8000-0xBFFFF     Color text mode buffer
 *     0xC0000-0xC7FFF     Video BIOS (typically)
 *     0xC8000-0xEFFFF     Mapped hardware & Misc (like EEPROM)
 *     0xF0000-0xFFFFF     Motherboard BIOS
 * 
 * Upper Memory Layout:
 * 0x00100000-0x00EFFFFF   RAM (contains kernel and possible modules/multiboot struct)
 * 0x00F00000-0x00FFFFFF   ISA Memory Hole (possible memory mapped hardware) !! CHECK
 * 0x01000000-0xC0000000   RAM
 * 0xC0000000-0xFFFFFFFF   Various, may contain memory mapped hardware !! CHECK
 * 0x100000000++           RAM + possible memory mapped hardware !! CHECK
 */

/* Temp GDT to enable full RAM for the kernel
 * When the kernel is initialized, it must create a new one
 * the new one must correctly define CODE/DATA/RODATA section for protection
 */
.section .gdt, "a", @progbits
.align 8
GDT32:
    .Null:                   # The NULL descriptor
        .long 0              # Limit & Base (low)
        .long 0              # Base (middle), Access, Granularity and Base (high)
    .Code:                   # The code descriptor
        .word 0xFFFF         # Limit (low)
        .word 0              # Base (low)
        .byte 0              # Base (middle)
        .byte 0x9A           # Access (exec/read)
        .byte 0xCF           # Granularity
        .byte 0              # Base (high)
    .Data:                   # The data descriptor
        .word 0xFFFF         # Limit (low)
        .word 0              # Base (low)
        .byte 0              # Base (middle)
        .byte 0x92           # Access (read/write)
        .byte 0xCF           # Granularity
        .byte 0              # Base (high)
GDT32_end:
GDT32_pointer:
    .word (GDT32_end - GDT32 - 1)   # Limit
    .long GDT32                     # Base

/* Multiboot header v2 */
.section .multiboot, "a", @progbits
multiboot_header:
    .long 0xE85250D6 /* Magic */
    .long 0 /* Arch */
    .long multiboot_header_end - multiboot_header /* Size of the multiboot header */
    .long -(0xE85250D6 + 0 + (multiboot_header_end - multiboot_header)) /* Checksum : the sum of previous and it must be 0 */
.mbi_tag_start:
    .short 0
    .short 0
    .long .mbi_tag_end - .mbi_tag_start
    .long 1
    .long 2
    .long 3
    .long 4
    .long 6
    .long 8
    .long 10
.mbi_tag_end:
    .align 8
.console_tag_start:
    .short 4
    .short 0
    .long .console_tag_end - .console_tag_start
    .long 2
.console_tag_end:
    .align 8
.module_tag_start:
    .short 6
    .short 0
    .long .module_tag_end - .module_tag_start
.module_tag_end:
    .align 8
    .short 0
    .short 0
    .long 8
multiboot_header_end:

.section .loader, "ax", @progbits
.global _entry32
.type _entry32, @function
.extern kmain
_entry32:
    /* Welcom, normally you are in protected mode (32-bit), some register
     * have been initialized for you like that :
     * EAX: multiboot magic number
     * EBX: 32-bit physical adresse to the multiboot info strucutre
     * CS: 32-bit read/execute code segment (undefined)
     * DS/ES/FS/GS/SS: 32-bit read/write data segment (undefined)
     * CR0: Bit 31 (PG) cleared, bit 0 (PE) is set, other are undefined
     * EFLAGS: Bit 17 (VM) and bit 9 (IF) cleared, other are undefined
     * ESP: invalide, define a stack as soon as needed (or override previous)
     * GDTR: invalide, you must setup a valide GDT table before using it
     * IDTR: invalide, you must provide an IDT before enable interruption
     * A20 gate is enabled.
     */
     cli
     cld

    /* Load GDT and clear segments registers */
    lgdt GDT32_pointer
    ljmp $0x8, $(Lreset)
Lreset:
    mov $0x10, %edx
    mov %edx, %ds
    mov %edx, %es
    mov %edx, %fs
    mov %edx, %gs
    mov %edx, %ss
    
    /* Enable stack and reset EFLAGS */
    mov $stack_top, %esp
    mov %esp, %ebp
    pushl $0
    popf
    
    /* Save multiboot informations */
    push %ebx
    push %eax

    call init_kernel

    /* Install page directory (in %eax because call return into it) */
    mov %eax, %cr3

    /* Save the page directory address */
    push %eax

    /* Enable the FPU and pagination with write protection */
    mov %cr0, %eax
    and $0xFFFB, %ax // clear EM
    or $0x80010032, %eax // set MP, NE, ET, WP and PG
    mov %eax, %cr0

    /* Initialize the FPU */
    fninit
    
    /* Jump far into the half-high kernel */
    lea _high_entry, %eax
    jmp *%eax
.size _entry32, . - _entry32

.text
.global _halt
.type _halt, @function
.type _high_entry, @function
_high_entry:
    call kmain
_halt:
Lhang:
    cli
    hlt
    jmp Lhang
