#![no_std] // don't link the Rust standard library
#![no_main] // disable all Rust-level entry points (except when we are testing)

#![feature(custom_test_frameworks)]
#![test_runner(substance_framework::test_runner)]

#![reexport_test_harness_main = "test_main"]

//////////////////////////////////////////////

#[macro_use]
extern crate static_assertions;

#[cfg(test)]
#[macro_use]
extern crate substance_framework;
#[cfg(test)]
#[macro_use]
extern crate substance_macro;

//////////////////////////////////////////////

#[macro_use]
pub mod arch;

#[macro_use]
pub mod klib;

//////////////////////////////////////////////

#[no_mangle]
pub extern "C" fn kmain() -> ! {
    loop {};
}

//////////////////////////////////////////////

#[cfg(test)]
mod tests {
    #[substance_test]
    fn dummy_test() {
        sf_assert!(true);
    }
}
