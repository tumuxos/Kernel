assert_cfg!(any(target_arch = "x86", target_arch = "i686"), "There is only support for x86/i686 CPU");

#[cfg(any(target_arch = "x86", target_arch = "i686"))]
#[macro_use]
pub mod x86;